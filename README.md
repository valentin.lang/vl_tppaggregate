# VL_TPPAGGREGATE

# TD MongoDB - Aggregation


Sujet : A l'aide d'un jeu de données et des explications
vues en cours veuillez trouver une nouvelle
opération d'aggregation issue de la documentation
en ligne de MongoDB, et intégrez -la dans une
requête d'agregation complète pour illustrer son
fonctionnement.


## Création d'un container mongo

Afin de pouvoir réaliser ce tp nous allons utiliser docker et récupérer sur le docker hub une image officiel de mongo pour docker.

Cela nous permettra d'avoir rapidement un environnement pour utiliser mongo

## Installation de docker et docker engine.

Docker et docker engine on besoin d'être installer sur windows afin de nous permettre de pouvoir créer des containers et utiliser les technologies que nous souhaitons(ici mongo)

téléchargement et installation Docker depuis le site officiel de Docker.

Une fois installer : 
```bash
docker -v
Docker version 24.0.7, build afdd53b
```


Ajout du container mongo : 

```bash
docker run --name tp-aggregate -d mongo
```

Ajout du container mongo express  pour gérer ma base de données MongoDB en interface graphique :

```bash
docker run --name tp-mongo-express -d -p 8081:8081 --link tp-aggregate:mongo -e ME_CONFIG_MONGODB_SERVER=tp-aggregate mongo-express
```

## Nous allons maintenant insérer le jeu de donnée dans notre container : 

Copies des Fichiers dans le Conteneur :

```bash 
docker cp C:/Users/valen/OneDrive/Bureau/movies-refs.json tp-aggregate:/tmp/movies.refs.json
Successfully copied 70.7kB to tp-aggregate:/tmp/movies.refs.json
docker cp C:/Users/valen/OneDrive/Bureau/movies-refs.json tp-aggregate:/tmp/artists.json
Successfully copied 70.7kB to tp-aggregate:/tmp/artists.json
```

Importez les Fichiers dans MongoDB :

```bash 
docker exec -it tp-aggregate mongoimport --db BDD_aggregate --collection movies --file /tmp/movies.refs.json --jsonArray

2024-01-07T21:14:38.697+0000    connected to: mongodb://localhost/
2024-01-07T21:14:38.720+0000    88 document(s) imported successfully. 0 document(s) failed to import.


docker exec -it tp-aggregate mongoimport --db BDD_aggregate --collection artists --file /tmp/artists.json --jsonArray


2024-01-07T21:15:01.284+0000    connected to: mongodb://localhost/
2024-01-07T21:15:01.305+0000    88 document(s) imported successfully. 0 document(s) failed to import.

```

Ensuite nous vérifions que la base de donnée BDD aggregate existe et qu'il existe bien du contenue : 

![](2024-01-07-22-16-25.png)

Maintenant nous allons faire la pipeline d'integration, nous n'avons pas le droit d'utiliser les opérateurs suivants : 
```bash
db.movies.aggregate([
{ $match: { "year": { $gte: 1975 } }},
{ $group: { "_id" : "$director.last_name" , "total":{ $sum:1} } },
{ $sort : { total : -1 } }
])
```

## Commande d'Agrégation MongoDB :

Dans cette section, nous décrirons comment effectuer une requête d'agrégation sur notre base de données MongoDB BDD_aggregate dans le conteneur Docker tp-aggregate. Cette requête spécifique a pour but de lister tous les films de la collection movies, en incluant leur titre, année de sortie, et le nombre d'acteurs pour chaque film.

Connection au shell MongoDB dans le conteneur Docker en utilisant la commande suivante :

docker exec -it tp-aggregate mongosh


Sélection de la Base de Données :

use BDD_aggregate

Exécution de la Commande d'Agrégation :

Pour exécuter la requête d'agrégation, tapons la commande suivante :

```bash
db.movies.aggregate([
    {
        $project: {
            title: 1,
            year: 1,
            numberOfActors: {
                $size: {
                    $ifNull: ["$actors", []]
                }
            }
        }
    }
])
```


Explication de la commande : 

Cette requête vous permet d'obtenir un aperçu rapide du nombre d'acteurs impliqués dans chaque film de votre collection movies, en plus de leur titre et année de sortie.

Nous retrouvons 3 opérateurs : 

## $project :

Cet opérateur est utilisé pour sélectionner ou transformer les champs qui apparaîtront dans le document de sortie. Ici, nous conservons les champs title (titre du film) et year (année de sortie du film), et nous ajoutons un nouveau champ numberOfActors.

## $size : 

Cet opérateur est utilisé pour calculer le nombre d'éléments dans un tableau. Dans notre cas, il compte le nombre d'acteurs dans chaque film.

## $ifNull :

Cet opérateur est utilisé pour vérifier si le champ actors existe et n'est pas null. Si le champ actors est absent ou null, il retourne un tableau vide, ce qui garantit que $size fonctionne correctement et renvoie 0 dans ces cas.

title: 1 et year: 1 permettent de sélectionner ces champs spécifiques pour qu'ils apparaissent dans les documents résultants de l'étape d'agrégation $project.